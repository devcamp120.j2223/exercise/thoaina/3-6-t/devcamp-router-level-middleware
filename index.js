const express = require("express");
const courseRouter = require("./app/routers/courseRouter");
const reviewRouter = require("./app/routers/reviewRouter");

const app = new express();

app.use(express.json());

app.use(express.urlencoded({
    urlencoded: true,
}));

const port = 8000;

app.get(`/`, (req, res) => {
    let today = new Date();
    res.status(200).json({
        message: `Hôm nay là ngày ${today.getDate()}`,
    })
})

app.use("/", courseRouter);
app.use("/", reviewRouter);

app.listen(port, () => {
    console.log(`App chạy trên cổng ${port}`);
})