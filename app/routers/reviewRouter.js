const express = require("express");
const reviewMiddleware = require("../middlewares/reviewMiddleware");
const reviewRouter = express.Router();

reviewRouter.use(reviewMiddleware);

reviewRouter.get("/reviews", (req, res) => {
    console.log("Get all reviews");
    res.json({
        message: "Get all reviews",
    })
})

reviewRouter.get("/reviews/:reviewId", (req, res) => {
    let id = req.params.reviewId;
    console.log("Get reviewId: " + id);
    res.json({
        message: "Get reviewId " + id,
    })
})

reviewRouter.put("/reviews/:reviewId", (req, res) => {
    let id = req.params.reviewId;
    let body = req.params.body;
    console.log("Get reviewId: " + id);
    res.json({
        message: { id, ...body },
    })
})

reviewRouter.post("/reviews", (req, res) => {
    let body = req.body;
    console.log("Create a review");
    console.log(body);
    res.json({
        ...body,
    })
})

reviewRouter.delete("/reviews/:reviewId", (req, res) => {
    let id = req.params.reviewId;
    console.log("Delete a review " + id);
    res.json({
        message: "delete a review " + id,
    })
})

module.exports = reviewRouter;