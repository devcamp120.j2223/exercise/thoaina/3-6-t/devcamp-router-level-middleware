const express = require("express");
const courseMiddleware = require("../middlewares/courseMiddleware");
const courseRouter = express.Router();

courseRouter.use(courseMiddleware);

courseRouter.get("/coures", (req, res) => {
    console.log("Get all courses");
    res.json({
        message: "Get all courses",
    })
})

courseRouter.get("/coures/:courseId", (req, res) => {
    let id = req.params.courseId;
    console.log("Get courseId: " + id);
    res.json({
        message: "Get courseId " + id,
    })
})

courseRouter.put("/coures/:courseId", (req, res) => {
    let id = req.params.courseId;
    let body = req.params.body;
    console.log("Get courseId: " + id);
    res.json({
        message: { id, ...body },
    })
})

courseRouter.post("/coures", (req, res) => {
    let body = req.body;
    console.log("Create a course");
    console.log(body);
    res.json({
        ...body,
    })
})

courseRouter.delete("/coures/:courseId", (req, res) => {
    let id = req.params.courseId;
    console.log("Delete a course " + id);
    res.json({
        message: "delete a course " + id,
    })
})

module.exports = courseRouter;